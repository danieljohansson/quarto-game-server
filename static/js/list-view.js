var list = document.querySelector('.game-list');

function create_list_item(game) {
    var el = document.createElement('li');
    var p0 = '<span class="player">' + game.players[0] + '</span>';
    var p1 = '<span class="player">' + game.players[1] + '</span>';
    if (game.players[0] && game.players[1]) {
        el.innerHTML = p0 + ' vs. ' + p1;
    } else if (game.players[0]) {
        el.innerHTML = p0 + ' waiting for an opponent';
    } else if (game.players[1]) {
        el.innerHTML = p1 + ' waiting for an opponent';
    } else {
        el.textContent = 'Waiting for players to join';
    }
    var link = document.createElement('a');
    link.textContent = 'View';
    link.href = '/games/' + game.game_id;
    el.appendChild(link);
    return el;
}

function render_list(game_list) {
    if (game_list.length== 0) {
        list.innerHTML = '<li>There are no games yet.</li>';
    } else {
        for (game of game_list) {
            list.appendChild(create_list_item(game));
        }
    }
}

function get_game_list() {
    var xhr = new XMLHttpRequest();
    xhr.addEventListener('load', function () {
        var game_list = JSON.parse(this.responseText);
        render_list(game_list);
    });
    xhr.open('GET', '/api/v1/games');
    xhr.send();
}

get_game_list();
