"""An example Quarto game server client.

This is an example client called Randy. He is not too concerned by winning. He
places every piece randomly, and chooses the next one without thinking. You
should really be able to win a game against Randy.
"""
import requests
import json
import time
import random
import argparse

from tornado import gen
from tornado.ioloop import IOLoop
from tornado.websocket import websocket_connect
from tornado.httputil import HTTPHeaders
from tornado.httpclient import HTTPRequest

def select_next(state):
    """Pick a piece by random from the remaining ones."""
    rem = state['remaining']
    piece = rem[random.randrange(0, len(rem))]
    return piece

def place_piece(state):
    """Pick an empty field of the board by random."""
    board = state['board']
    start_index = random.randrange(0, 16)
    cols = list('ABCD')
    for i in range(0, 16):
        index = (start_index + i) % 16
        col = cols[index // 4]
        row = index % 4
        if board[col][row] == None:
            return col + str(row + 1)
    else:
        raise Exception('Board is full.')

class HttpQuartoPlayer(object):
    """docstring for HttpQuartoPlayer"""
    def __init__(self, url):
        self.url = url
        self.player = {'name': 'Randy'}
        self.join()

    def join(self):
        r = requests.post(self.url + '/join', json={'name': self.player['name']})
        r.raise_for_status()
        self.player.update(r.json())

        print('Joined as {}'.format(self.player))

    def wait_for_turn(self):
        """Poll the server with increasing time intervals."""
        for i in range(0, 21):
            r = requests.get(self.url)
            r.raise_for_status()
            if r.json()['state'] == 'finished':
                return False
            if r.json()['active_player'] == self.player['index']:
                return True
            duration = 1.2**i - 1
            print('Waiting {:.3} seconds.'.format(duration))
            time.sleep(duration)
        else:
            raise Exception('Timed out after 3.5 minutes.')

    def make_move(self, state):
        # Place piece
        if state['next_piece'] != None:
            position = place_piece(state)
            print('Placing piece at {}.'.format(position))
            r = requests.post(self.url + '/piece-position',
                json={'position': position},
                cookies={'player-token': self.player['token']})
            r.raise_for_status()
            if r.json()['state'] != 'playing':
                return

        # Select next
        next_piece = select_next(state)
        print('Selecting {} as next piece.'.format(next_piece))
        r = requests.post(self.url + '/next-piece',
            json={'piece': next_piece},
            cookies={'player-token': self.player['token']})
        r.raise_for_status()

    def play(self):
        while self.wait_for_turn():
            # Get state
            r = requests.get(self.url)
            r.raise_for_status()
            state = r.json()

            # Make move
            self.make_move(state)

class WebSocketQuartoPlayer(HttpQuartoPlayer):
    """docstring for WebSocketQuartoPlayer"""
    def __init__(self, *args, **kwargs):
        super(WebSocketQuartoPlayer, self).__init__(*args, **kwargs)

    def make_move(self, state):
        if state['next_piece'] != None:
            # Place piece
            position = place_piece(state)
            print('Placing piece at {}.'.format(position))
            self.ws.write_message(json.dumps({
                'method': 'piece-position',
                'data': {
                    'position': position
                }
            }))
        else:
            # Select next
            next_piece = select_next(state)
            print('Selecting {} as next piece.'.format(next_piece))
            self.ws.write_message(json.dumps({
                'method': 'next-piece',
                'data': {
                    'piece': next_piece
                }
            }))

    def play(self):
        headers = HTTPHeaders({
            'cookie': 'player-token=' + self.player['token']
        })
        ws_url = 'ws' + self.url[4:] + '/message-channel'
        ws_request = HTTPRequest(url=ws_url, headers=headers)
        @gen.engine
        def run():
            self.ws = yield websocket_connect(ws_request)

            while True:
                message = yield self.ws.read_message()
                if message == None:
                    break
                message_json = json.loads(message)
                if message_json['status'] != 'ok':
                    print('Error: {}'.format(message_json['error']))
                    break
                state = message_json['data']
                if state['state'] == 'finished':
                    break
                if state['active_player'] == self.player['index']:
                    self.make_move(state)
            IOLoop.instance().stop()

        IOLoop.instance().add_callback(run)
        IOLoop.instance().start()

if __name__ == '__main__':
    # Parse commandline arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--websocket", help="Use WebSocket interface",
        action="store_true")
    parser.add_argument("url", help="The URL of the game API")
    url = parser.parse_args().url
    use_websocket = parser.parse_args().websocket

    # Play
    t0 = time.time()
    if use_websocket:
        WebSocketQuartoPlayer(url).play()
    else:
        HttpQuartoPlayer(url).play()
    t1 = time.time()
    print('Game duration: {:0.1f} ms'.format((t1 - t0) * 1000.0))
