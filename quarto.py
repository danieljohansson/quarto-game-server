
import re
import string
import random

class GameError(Exception):
    """Base class for exceptions in this module."""
    def __init__(self, message):
        self.message = message

class GameValueError(GameError):
    """Raised when invalid values or parameters are passed to the module."""
    code = 400

class GamePlayError(GameError):
    """Raised when an forbidden or invalid action is attempted."""
    code = 403

class GameNotFound(GameError):
    """Raised when a non-extistent game is requested."""
    code = 404

def random_string(N):
    chars = string.ascii_lowercase + string.digits
    return ''.join(random.choice(chars) for _ in range(N))

class GameList(object):
    """docstring for GameList"""
    def __init__(self):
        self.games = {}

    def new_game(self):
        game_id = random_string(8)
        while game_id in self.games:
            game_id = random_string(8)
        self.games[game_id] = Game()
        return game_id

    def get(self, game_id):
        if not game_id in self.games:
            raise GameNotFound('Invalid game ID.')
        return self.games[game_id]

    def get_list(self):
        # TODO: Return more useful info.
        return [
            {'game_id': k, 'players': [
                None if p == None else p.name for p in v.players
            ]}
            for k, v
            in self.games.items()
        ]

def new_piece_set():
    piece_set = [];
    for i in range(0, 16):
        piece_set.append([
            i // 1 % 2,
            i // 2 % 2,
            i // 4 % 2,
            i // 8 % 2,
        ]);
    return piece_set

def is_valid_piece(piece):
    if not type(piece) is list:
        return False
    if len(piece) != 4:
        return False
    for item in piece:
        if item != 0 and item != 1:
            return False
    return True

class Player(object):
    """docstring for Player"""
    def __init__(self, name):
        self.name = name
        self.token = random_string(16)

# Game states
_OPEN = 0
_PLAYING = _OPEN + 1
_FINISHED = _PLAYING + 1

class Game(object):
    """docstring for Game"""
    coordinate_pattern = re.compile("^[A-D][1-4]$")
    piece_pattern = re.compile("^[01]{4}$")
    row_table = [[0, 1, 2, 3],
                 [0, 4, 8, 12],
                 [0, 5, 10, 15],
                 [6, 7, 4, 5],
                 [6, 10, 14, 2],
                 [6, 9, 12, 3],
                 [9, 10, 11, 8],
                 [9, 13, 1, 5],
                 [15, 12, 13, 14],
                 [15, 3, 7, 11]];

    def __init__(self):
        self.state = _OPEN
        self.players = [None, None]
        self.active_player = None
        self.winner = None
        self.board = 16 * [None]
        self.remaining = new_piece_set()
        self.next_piece = None

    def active_player_token(self):
        return self.players[self.active_player].token;

    def play_validate(self, player_token):
        if self.state < _PLAYING:
            raise GamePlayError('The game has not started.');
        if self.state > _PLAYING:
            raise GamePlayError('The game is finished.');
        if player_token == None:
            raise GameValueError('No player token provided.')
        if player_token != self.active_player_token():
            raise GamePlayError('Not your turn.')

    def player_join(self, name):
        if self.state > _OPEN:
            raise GamePlayError('The game is full.')
        p = Player(name)
        if self.players[0] == None:
            index = 0
        else:
            index = 1
            # Registration of two players complete.
            self.state = _PLAYING
            # Who starts is random.
            self.active_player = random.randrange(2)
        self.players[index] = p;
        return {
            'index': index,
            'token': p.token
        }

    def select_next(self, piece_str, player_token):
        self.play_validate(player_token)
        if not self.piece_pattern.match(piece_str):
            raise GameValueError('Invalid piece.')
        if self.next_piece != None:
            raise GamePlayError('The current piece must be placed first.')
        piece = [int(i) for i in list(piece_str)]
        try:
            self.remaining.remove(piece)
        except ValueError:
            raise GamePlayError('This piece is not remaining.');
        self.next_piece = piece
        # Other player's turn.
        self.active_player = 0 if self.active_player == 1 else 1

    def place_piece(self, coordinates, player_token):
        self.play_validate(player_token)
        if not self.coordinate_pattern.match(coordinates):
            raise GameValueError('Invalid coordinates.')
        if self.next_piece == None:
            raise GamePlayError('Next piece is not selected.')
        xy = list(coordinates)
        x = 'ABCD'.index(xy[0])
        y = int(xy[1]) - 1
        index = 4 * x + y;
        if self.board[index] == None:
            self.board[index] = self.next_piece
            self.next_piece = None
        else:
            raise GamePlayError('The field is full.')
        if self.is_won():
            self.state = _FINISHED
            self.winner = self.active_player
        elif self.board_is_full():
            self.state = _FINISHED

    def is_won(self):
        for row in self.row_table:
            pieces = [self.board[i] for i in row]
            if pieces.count(None) > 0:
                continue
            for prop in range(4):
                ps = [pieces[j][prop] for j in range(4)]
                if ps[0] == ps[1] == ps[2] == ps[3]:
                    return True
        return False

    def board_is_full(self):
        for i in range(16):
            if self.board[i] == None:
                return False
        return True

    def get_state(self):
        def convert_piece(piece):
            if piece == None:
                return None
            else:
                return ''.join([str(x) for x in piece])
        def convert_piece_list(piece_list):
            return [convert_piece(p) for p in piece_list]
        def convert_board(board):
            temp = {}
            for i, col in enumerate(list('ABCD')):
                temp[col] = convert_piece_list(board[4*i:4*i+4])
            return temp

        players = [None if p == None else p.name for p in self.players]
        return {
            'state': ['open', 'playing', 'finished'][self.state],
            'players': players,
            'active_player': self.active_player,
            'winner': self.winner,
            'board': convert_board(self.board),
            'remaining': convert_piece_list(self.remaining),
            'next_piece': convert_piece(self.next_piece),
        }
