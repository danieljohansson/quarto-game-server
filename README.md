# Quaro game server

This is a simple game server where AIs (and/or humans) can play the [Quarto board game](https://en.wikipedia.org/wiki/Quarto_(board_game)). An example client called Randy is included to demonstrate how the API can be used.

## Contents
* [Getting started](#getting-started)
    * [Requirements](#requirements)
    * [Running the server](#running-the-server)
    * [Playing a game with the example client](#playing-a-game-with-the-example-client)
* [API reference (v1)](#api-reference-v1)

## Getting started

### Requirements
The server is written in Python (runs with Python 2.7 or 3.5). The following packages are required for the server and example client.

* flask (0.11.1)
* requests (2.11.1)
* tornado (4.4.1)

### Running the server
```shell_session
$ python server.py
```
By default this starts a server at <http://localhost:5000>. You can navigate there with a web browser.

### Playing a game with the example client
First you need to create a new game using the HTTP API. For example
```shell_session
$ curl http://localhost:5000/api/v1/games -X POST -H "Content-Type: application/json"
```
This returns an ID for the new game. Pass that to the client as a part of the game URL. For example
```shell_session
$ python example-client.py --websocket http://localhost:5000/api/v1/games/5k0bjnsk
```
Start two instances to let them play each other.

## API reference (v1)

The server API has two parts, an HTTP interface and a WebSocket interface. The primary way of communication is over HTTP, but the in-game API methods are also available through a WebSocket. It is encouraged to use the WebSocket interface where possible. Both interfaces use the JSON data format.

### Contents
* [Parameters](#parameters)
* [HTTP interface](#http-interface)
    * [Create a new game](#create-a-new-game)
    * [Get a list of current games](#get-a-list-of-current-games)
    * [Join an existing game](#join-an-existing-game)
    * [Get the current game state](#get-the-current-game-state)
    * [Select the next piece to play](#select-the-next-piece-to-play)
    * [Place a piece on the board](#place-a-piece-on-the-board)
* [WebSocket interface](#websocket-interface)
    * [Endpoint](#endpoint)
    * [Listen](#listen)
    * [Select the next piece to play](#select-the-next-piece-to-play-1)
    * [Place a piece on the board](#place-a-piece-on-the-board-1)
    * [Errors](#errors)

### Parameters

| Parameter | Description | Example
|---|---|---
| `<game-id>` | a string identifying a game | `"5k0bjnsk"`
| `<player-token>` | a string used to authenticate a player within a game | `"1ocxla8mx5eqkhzb"`
| `<player>` | a string with a player's name, or `null` if there is no player | `"Randy"`
| `<piece>` | a string representing a piece, or `null` if there is no piece | `"0101"`
| `<position>` | a string identifying a position on the board | `"B3"`

### HTTP interface

The API uses conventional HTTP status codes to signal the success or faliure of a request. There are three main ranges of status codes.

| Range | Description        | Example cause
|-------|--------------------|--------------
| 2xx   | Successful request | Everything worked as expected.
| 4xx   | Client error       | Trying to make a move when you're not the active player. A missing parameter or otherwise malformed request.
| 5xx   | Server error       | Server overloaded or a server bug.

#### Create a new game

    POST /api/v1/games

Example request

```shell_session
$ curl http://localhost/api/v1/games \
    -X POST \
    -H "Content-Type: application/json"
```

Response
```json
{"game_id": <game-id>}
```

<!--Errors-->

#### Get a list of current games

    GET /api/v1/games

Example request

```shell_session
$ curl http://localhost/api/v1/games
```

Response

```json
[
    {
        "game_id": <game-id>,
        "players": [<player>, <player>]
    },
    ...
]
```

<!--Errors-->

#### Join an existing game

    POST /api/v1/games/<game-id>/join

Request data
```json
{"name":<player>}
```

Example request

```shell_session
$ curl http://localhost/api/v1/games/5k0bjnsk/join \
    -X POST \
    -d '{"name":"Randy"}' \
    -H "Content-Type: application/json"
```

Response

```json
{
    "index": 0 | 1,
    "token": <player-token>
}
```

<!--Errors-->

#### Get the current game state

    GET /api/v1/games/<game-id>

Example request

```shell_session
$ curl http://localhost/api/v1/games/5k0bjnsk
```

Response

```json
{
    "next_piece": <piece>,
    "active_player": 0 | 1 | null,
    "winner": 0 | 1 | null,
    "players": [<player>, <player>],
    "state": "open" | "playing" | "finished",
    "board": {
        "A": [<piece>, <piece>, <piece>, <piece>],
        "B": [<piece>, <piece>, <piece>, <piece>],
        "C": [<piece>, <piece>, <piece>, <piece>],
        "D": [<piece>, <piece>, <piece>, <piece>]
    },
    "remaining": [
        <piece>,
        ...
    ]
}
```

<!--Errors-->

#### Select the next piece to play

    POST /api/v1/games/<game-id>/next-piece

Request data
```json
{"piece": <piece>}
```

This method requires that the "player-token" cookie of the request is set to the active player's `<player-token>`.

Example request

```shell_session
$ curl http://localhost/api/v1/games/5k0bjnsk/next-piece \
    -X POST \
    -d '{"piece":"0101"}' \
    -H "Cookie: player-token=1ocxla8mx5eqkhzb" \
    -H "Content-Type: application/json"
```

Response

A successful request returns the updated game state the same way as `GET /api/v1/games/<game-id>`.

#### Place a piece on the board

    POST /api/v1/games/<game-id>/piece-position

Request data
```json
{"position": <position>}
```

This method requires that the "player-token" cookie of the request is set to the active player's `<player-token>`.

Example request

```shell_session
$ curl http://localhost/api/v1/games/5k0bjnsk/piece-position \
    -X POST \
    -d '{"position":"B3"}' \
    -H "Cookie: player-token=1ocxla8mx5eqkhzb" \
    -H "Content-Type: application/json"
```

Response

A successful request returns the updated game state the same way as `GET /api/v1/games/<game-id>`.

<!--Errors-->

### WebSocket interface

Each game has a WebSocket interface providing a real-time message channel. Viewers can subscribe to a stream of game events, and players can additionally post their moves on this channel.

#### Endpoint

    /api/v1/games/<game-id>/message-channel

#### Listen

To listen to the message channel you open a WebSocket connection to the endpoint URL above.

On open you will get a message with the current game state. After every event in the game a message will be sent with the updated game state. This includes players joining the game, selecting a piece, or placing a piece on the board.

The message looks as follows.
```json
{
    "status": "ok",
    "data": <data>
}
```
`<data>` has the same format as the response in the HTTP interface.

JavaScript example of subscribing to the channel.

```javascript
var ws = new WebSocket('ws://localhost/api/v1/games/5k0bjnsk/message-channel');
ws.onmessage = function (event) {
    console.log(event);
};
```

#### Select the next piece to play

Message
```json
{
    "method": "next-piece",
    "data": {
        "piece": <piece>
    }
}
```

This method requires that the "player-token" cookie is set to the active player's `<player-token>` when opening the WebSocket.

JavaScript example of posting a message to the channel.

```javascript
document.cookie = 'player-token=1ocxla8mx5eqkhzb; path=/'; // If not already set.
var ws = new WebSocket('ws://localhost/api/v1/games/5k0bjnsk/message-channel');
ws.onopen = function() {
    var message = {
        method: 'next-piece',
        data: {piece: '0101'}
    };
    ws.send(JSON.stringify(message));
};
```

#### Place a piece on the board

Message
```json
{
    "method": "piece-position",
    "data": {
        "position": <position>
    }
}
```

This method requires that the "player-token" cookie is set to the active player's `<player-token>` when opening the WebSocket.

See previous method for an example.

#### Errors

If a player posts a message when it is not their turn or there is some other error, the server will respond with an error message. The message includes an `error` object describing the issue.

Example error message
```json
{
    "status": "error",
    "error": {
        "code": 403,
        "description": "Not your turn."
    }
}
```
