# coding=utf8

import argparse
import re

parser = argparse.ArgumentParser()
parser.add_argument("file",
    help="The Markdown file to generate a table of contents for")
file = parser.parse_args().file

id_list = []

def title_to_id(title):
    """Convert title text to ID used in links.

    The IDs are generated from the content of the header according to the
    following rules:

    1. All text is converted to lowercase
    2. All non-word text (e.g., punctuation, HTML) is removed
    3. All spaces are converted to hyphens
    4. Two or more hyphens in a row are converted to one
    5. If a header with the same ID has already been generated, a unique
       incrementing number is appended, starting at 1.
    """
    lower = title.lower()
    stripped = re.sub('[^0-9A-zÀ-ÿ -]', '', lower)
    hyphens = stripped.replace(' ', '-')
    title_id = re.sub('-+', '-', hyphens)

    def id_and_counter(title_id, prev):
        return (title_id + '-' + str(prev) if prev > 0 else title_id)

    # Number of previous occurences
    prev = 0
    while id_and_counter(title_id, prev) in id_list:
        prev = prev + 1

    title_id = id_and_counter(title_id, prev)
    id_list.append(title_id)
    return title_id

def title_indent(line):
    level = len(re.search('^#+', line).group(0))
    return '    ' * (level - 1)

f = open(file, 'r')

for line in f:
    if line[0] == '#':
        line = line.strip()
        title = line.lstrip(' #')
        indent = title_indent(line)
        title_id = title_to_id(title)
        print('{}* [{}](#{})'.format(indent, title, title_id))
