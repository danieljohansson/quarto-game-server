from flask import (
    abort,
    Flask,
    jsonify,
    redirect,
    render_template,
    request,
    Response,
    url_for,
)

from tornado.wsgi import WSGIContainer
from tornado.web import Application, FallbackHandler
from tornado.websocket import WebSocketHandler
from tornado.ioloop import IOLoop

import json

import quarto

app = Flask(__name__)

games = quarto.GameList()

# -----------------------------------------------------------------
#      Index page
# -----------------------------------------------------------------
@app.route('/')
def index():
    return redirect(url_for('list_view'))

# -----------------------------------------------------------------
#      Game view
# -----------------------------------------------------------------
@app.route('/games')
def list_view():
    return render_template('list-view.html')

@app.route('/games/<string:game_id>')
def game_view(game_id):
    games.get(game_id)
    return render_template('game-view.html', game_id=game_id)

# -----------------------------------------------------------------
#      Common API helpers
# -----------------------------------------------------------------
def next_piece(game_id, data, token):
    if not data or not 'piece' in data:
        raise quarto.GameValueError(
            'Required parameter "piece" was not provided.')
    games.get(game_id).select_next(data['piece'], token)
    websocket_post_state(WebSocket.connections.get(game_id, []))

def piece_position(game_id, data, token):
    if not data or not 'position' in data:
        raise quarto.GameValueError(
            'Required parameter "position" was not provided.')
    games.get(game_id).place_piece(data['position'], token)
    websocket_post_state(WebSocket.connections.get(game_id, []))

# -----------------------------------------------------------------
#      HTTP API
# -----------------------------------------------------------------

game_root = '/api/v1/games/<string:game_id>'

# Game error handler
# ------------------
@app.errorhandler(quarto.GameError)
def handle_game_error(error):
    resp = jsonify({'message': error.message})
    resp.status_code = error.code
    return resp

# API endpoint handlers
# ---------------------
@app.route('/api/v1/games', methods=['GET', 'POST'])
def api_games():
    if request.method == 'POST':
        # Create a new game.
        game_id = games.new_game()
        return jsonify({'game_id': game_id})
    else:
        # Return a list of all games.
        return jsonify(games.get_list())

@app.route(game_root)
def api_game_state(game_id):
    return jsonify(games.get(game_id).get_state())

@app.route(game_root + '/join', methods=['POST'])
def api_game_join(game_id):
    data = request.get_json()
    if not 'name' in data:
        raise quarto.GameValueError(
            'Required parameter "name" was not provided.')
    player = games.get(game_id).player_join(data['name'])
    websocket_post_state(WebSocket.connections.get(game_id, []))
    resp = jsonify(player)
    resp.set_cookie('player-token', player['token'])
    return resp

@app.route(game_root + '/next-piece', methods=['POST'])
def api_next(game_id):
    next_piece(game_id, data=request.get_json(),
        token=request.cookies.get('player-token'))
    return jsonify(games.get(game_id).get_state())

@app.route(game_root + '/piece-position', methods=['POST'])
def api_place(game_id):
    piece_position(game_id, data=request.get_json(),
        token=request.cookies.get('player-token'))
    return jsonify(games.get(game_id).get_state())

# -----------------------------------------------------------------
#      WebSocket API
# -----------------------------------------------------------------
class WebSocket(WebSocketHandler):
    connections = {}

    def open(self, game_id):
        try:
            games.get(game_id)
        except quarto.GameNotFound as error:
            self.close(code=error.code, reason=error.message)
        self.game_id = game_id
        if not game_id in self.connections:
            self.connections[game_id] = []
        self.connections[game_id].append(self)
        websocket_post_state([self])

    def on_message(self, message):
        token = self.get_cookie('player-token')
        message = json.loads(message)
        try:
            websocket_method_handler(message['method'], self.game_id,
                message['data'], token)
        except quarto.GameError as error:
            self.write_message(json.dumps({
                'status': 'error',
                'error': {
                    'code': error.code,
                    'description': error.message,
                },
            }))

    def on_close(self):
        self.connections[self.game_id].remove(self)
        if len(self.connections[self.game_id]) == 0:
            self.connections.pop(self.game_id)

def websocket_method_handler(method, game_id, data, token):
    if method == 'next-piece':
        next_piece(game_id, data, token)
    elif method == 'piece-position':
        piece_position(game_id, data, token)
    else:
        raise quarto.GameValueError('Invalid method.')

def websocket_post_state(connections):
    for connection in connections:
        state = games.get(connection.game_id).get_state()
        message = json.dumps({
            'status': 'ok',
            'data': state,
        })
        connection.write_message(message)

# -----------------------------------------------------------------
#      App init
# -----------------------------------------------------------------
if __name__ == '__main__':
    # A warning from the Tornado documentation:
    # WSGI is a synchronous interface, while Tornado’s concurrency model is
    # based on single-threaded asynchronous execution. This means that running
    # a WSGI app with Tornado’s WSGIContainer is less scalable than running
    # the same app in a multi-threaded WSGI server like gunicorn or uwsgi. Use
    # WSGIContainer only when there are benefits to combining Tornado and WSGI
    # in the same process that outweigh the reduced scalability.
    container = WSGIContainer(app)
    handlers = [
        (r'/api/v\d/games/([0-9a-z]+)/message-channel', WebSocket),
        (r'.*', FallbackHandler, {'fallback': container})
    ]
    server = Application(handlers, debug=True)
    server.listen(5000)
    IOLoop.instance().start()
